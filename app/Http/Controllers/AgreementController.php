<?php
// Our Controller 
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use App\Job;
use App\Helper;
use DB;
use App\Proposal;
use App\Mail\EmailVerificationMailable;
use ZipArchive;
use App\User;
use App\Profile;
use Storage;
use Response;
use Auth;
use Carbon\Carbon;
use App\Package;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Mail;
use App\SiteManagement;
use App\Mail\EmployerEmailMailable;
use App\Mail\FreelancerEmailMailable;
use App\EmailTemplate;
use App\Review;
// This is important to add here. 
use PDF;
  
class AgreementController extends Controller
{
    public function printPDF(Request $request,$slug)
    {
        $accepted_proposal = array();
        $job = Job::where('slug', $slug)->first();
        $assigneesign = $job->lancer_sign;
        $assignorsign = $job->employer_sign;

        $employer_name = Helper::getUserName($job->user_id);
        $accepted_proposal = Job::find($job->id)->proposals()->where('hired', 1)
            ->first();

        $freelancer_name = Helper::getUserName($accepted_proposal->freelancer_id);
        $employer_name = Helper::getUserName($job->user_id);

        $employer_firstname = User::find($job->user_id)->first_name;
        $freelancer_firstname = Helper::getUserfirstName($accepted_proposal->freelancer_id);
        // $freelancerid=User::find($freelancer_firstname)->user_id;
        $freeid = User::where('first_name', $freelancer_firstname)->first();
        $freelancer_address = Profile::find($freeid->id)->address;
        $emplid = User::where('first_name', $employer_firstname)->first();
        $employer_address = Profile::find($emplid->id)->address;

        
        $em_postal = Profile::find($emplid->id)->postal;
        $free_postal = Profile::find($freeid->id)->postal;




        $agreedate = date('M d Y');//Nov 15 2020
        $nagreedate = date('m-d-y');
        $nmonth = substr($nagreedate,0,2);
        // dd($agreedate);
        $year = substr($agreedate,7,4);
        $month = substr($agreedate,0,3);
        $day = substr($agreedate,4,2);
       // This  $data array will be passed to our PDF blade
       $data = [
            'title' => 'agreement',
            'heading' => 'ASSIGNMENT AGREEMENT FOR COPYRIGHTED VIDEO',
            'headingj' =>'著作権動画の割り当て契約 ',
            'content1' => 'THIS ASSIGNMENT is made this '.$day.'th day of '.$month.', '.$year.', (the “Effective date”) by and between '
                        .$employer_name.' (the “Assignor”) and '.$freelancer_name.' (the “Assignee”) (collectively, the “Parties”)',
            'content1j' =>'この割り当ては、'.$year.'年'.$nmonth.'月'.$day.'日に行われます（「有効日付」）'.$employer_name.'（「 譲渡人」）
                        および'.$freelancer_name.'（「譲受人」）（総称して「当事者」）の契約。',
            'content2' => 'WHEREAS, Assignor is the copyright holder and owner of all proprietary interest in '.$job->code.'(describe video)'.$job->title.' (the “Work”).',
            'content2j'=>'譲渡人は著作権所有者であり、譲受人は'.$job->code.','.$job->title.'[プロジェクト名]（「事業」）での成果物の
                        譲受人である。',
            'content3' => 'AND WHEREAS, Assignor  wishes  to  transfer  all  rights,  ownership  and  interest  in  the  finish 
                        product  of  Work  which Assignor  delivered,  including  the  copyright  and  all  other  intellectual 
                        property rights, to Assignee, under the terms set forth in this Agreement;',
            'content3j'=>'譲渡人は、本契約に定められた条件に基づいて、著作権およびその他すべての知的財産権を含め、
                        譲渡人が提供した作品の完成品に対するすべての権利、所有権、および利益を譲受人に譲渡すること
                        を希望します。',
            'content4' => 'NOW THEREFORE, in consideration of the mutual promises, covenants, warranties, and other good and valuable
                         consideration set forth herein, the Parties hereby agree as follows:',
            'content4j'=>'そのため、相互の約束、契約、保証、および本書に記載されているその他の有益で価値のある考慮
                        事項を考慮して、両当事者は以下のことに同意します。',
            'content5' =>'1.  Assignment of the Work. ',
            'content5j'=>'作業の割り当て',
            'content6' =>'Assignor  hereby  irrevocably  assigns  to Assignee  all  rights,  title  and  interest  to  the  Work, 
                        including  all  copyright  ownership  and  interest,  and  all  moral  rights  associated  with  the 
                        creation of the Work and all other intellectual property associated with the Work.   Assignee 
                        shall be the exclusive owner of the Work and of the copyright in the Work from the Effective 
                        Date forward, and shall have the exclusive right to secure registration of the copyright in the 
                        Work with the U.S. Copyright Office and shall have the exclusive right to secure registration 
                        of the copyright internationally.  No rights in the Work, or in the copyright in the Work, shall 
                        be retained by Assignor, nor shall there be any reversion of those rights to Assignor in the 
                        future.',
            'content6j'=>'譲渡人は、これにより、著作権の所有権と利益、および著作物の作成に関連するすべての
                        著作者人格権、および著作物に関連するその他すべての知的財産を含む、著作物に対するすべての
                        権利、権原、および利益を譲受人に取消不能に譲渡します。  譲受人は、発効日以降、著作物およ
                        び著作物の著作権の独占的所有者であり、米国著作権局への著作物の著作権の登録を確保する独占的
                        権利を有し、確保する独占的権利を有するものとします。 
                        国際的な著作権の登録。  作品の権利、または作品の著作権は、譲渡人によって保持されないもの
                        とします。また、将来、これらの権利が譲渡人に返還されることもありません。',
            'content7' =>'2.   Extend of copyright protection.',
            'content7j'=>'著作権の及ぶ範囲 ',
            'content8' =>'Works produced during the course of the project are not included in the agreement of the 
                        contract. In addition, this contract does not permit the use of works in the production process 
                        or the exercise of copyrights for products other than the delivered products.',
            'content8j'=>'プロジェクトの過程で製作された著作物は契約書の同意に含まれません。また、納品された製作物
                        以外に対する製作過程の著作物の使用、著作権の行使をこの契約書では認めません。',
            'content9' =>'3.  Payment.',
            'content9j'=>'支払い  ',
            'content10'=>'In  consideration  of  the  assignment  made  by  Assignor,  as  well  as  Assignor’s  promises, 
                        representations,  covenants  and  warranties  under  this  Agreement,  upon  execution  of  this 
                        Agreement Assignee shall pay to Assignor the amount of $XXX.00 which both parties agreed 
                        at project on domain.com.',
            'content10j'=>'譲渡人による譲渡、および本契約に基づく譲渡人の約束、表明、誓約および保証を考慮して、
                        譲受人は、本契約の締結時に、両当事者がドメインのプロジェクトで合意したXXX.00ドルを譲渡人
                        に支払うものとします。remo-worker.com ',
            'content11'=>'4.  Assignor’s Representations and Warranties.',
            'content11j'=>'譲渡人の表明および保証',
            'content12'=>'Assignor hereby represents and warrants the following:',
            'content12j'=>'譲渡人は、これにより以下を表明および保証します。',
            'content13'=>'a.  Assignor  has  the  legal  authority  to  grant  the  assignment  of  the  Work,  including  all 
                        copyright  rights  and  proprietary  interest  therein,  as  set  forth  in  Section  1.    No  other 
                        person or entity is required to consent to this assignment or to this Agreement for it to be 
                        valid and complete.',
            'content13j'=>'譲渡人は、１項に記載されているように、すべての著作権および所有権を含む作品の譲渡を許可
                        する法的権限を有します。他の個人または団体は、この譲渡または本契約が有効であることに同意す
                        る必要はありません。 この契約書が完全な同意を意味し、保証します。',
            'content14'=>'b.  There are currently no licenses outstanding granting any other person or entity the right to 
                        enjoy  or  lay  claim  to  any  copyright  rights  or  privileges  in  the Work,  other  than  those 
                        licenses listed in Appendix A, copies of which are attached as Exhibits.  Assignor will not 
                        attempt to grant any such licenses at any time in the future.   To the best of Assignor’s 
                        knowledge, the Work, and all copyright interest in the Work, is free and clear of any liens, 
                        security interests, or other encumbrances.',
            'content14j'=>'現在、付録Aに記載されているライセンス以外に、著作物の著作権または特権を享受または主張す
                        る権利を他の個人または団体に付与している不透明なライセンスはありません。これらのライセンスの
                        コピーは別紙として添付されています。 譲渡人は、将来いつまでもライセンスに関する権利を主張しよ
                        うとはしません。譲渡人の知る限り、著作物、および著作物のすべての著作権上の利益は、先取特権、
                        担保権、またはその他の負担はなく虚偽もありません。',
            'content15'=>'c.  To  the  best  of  Assignor’s  knowledge,  the  Work  does  not  infringe  upon  the  rights, 
                        copyright or otherwise, of any other person or entity.',
            'content15j'=>'譲渡人の知る限り、著作物は他の個人または団体の権利、著作権、またはその他を侵害するもの
                        ではありません。',
            'content16'=>'d. There are no claims currently pending or threatened, nor does Assignor have any reason 
                        to believe that any claims will be brought or threatened in the future, against Assignor’s 
                        right, ownership or interest in the Work.',
            'content16j'=>'現在紛争中または脅迫されている請求はなく、譲渡人は、著作物に対する譲渡人の権利、所有権、
                        または利 益に対して、将来、請求が提起または脅迫されるとことはないと誓います。 ',
            'content17'=>'5.  Indemnification.',
            'content17j'=>'補償',
            'content18'=>'Assignor  agrees  to  indemnify  and  hold  harmless Assignee  for  any  claims,  suits,  damages, 
                        actions, or other costs arising out any breach of Assignor’s warranties set forth in Section 3 
                        above.',
            'content18j'=>'譲渡人は、上記の３項に記載されている譲渡人の保証の違反に起因する請求、訴訟、損害、訴訟、
                        またはその他の費用について、譲受人の負担を補償し無害にすることに同意します。',
            'content19'=>'6.  Governing Law.',
            'content19j'=>'準拠法',
            'content20'=>'This Agreement shall be construed in accordance with, and governed in all respects by, the 
                        laws of the State of Nevada in USA, without regard to conflicts of law principles.',
            'content20j'=>'本契約は抵触法の原則に関係なく米国ネバダ州の法律に従って解釈され、あらゆる点で準拠する
                        ものとします。 ',
            'content21'=>'7.  Severability. ',
            'content21j'=>'可分性',
            'content22'=>'In  the  event  that  any  part  or  parts  of  this Agreement  shall  be  held  unenforceable  for  any 
                        reason,  the  remainder  of  this  Agreement  shall  continue  in  full  force  and  effect  as  if  the 
                        unenforceable part or parts were. If any provision of this Agreement is deemed invalid or 
                        unenforceable by any court of competent jurisdiction, and if limiting such provision would 
                        make the provision valid, then such provision shall be deemed to be construed as so limited.',
            'content22j'=>'本契約のいずれかの部分が、何らかの理由で執行不能とされた場合、本契約の残りの部分は執行
                        不能な部分関わらずに完全に効力を持ち続けるものとします。 本契約のいずれかの条項が管轄裁判所
                        によって無効または執行不能と見なされ、そのような条項を制限することでその条項が有効になる場合、
                        そのような条項はそのように制限されていると解釈されるものとします。 ',
            'content23'=>'8.  Counterparts. ',
            'content23j'=>'相対物 ',
            'content24'=>'This Agreement may be executed in several counterparts, each of which shall constitute an 
                        original and all of which, when taken together, shall constitute one agreement. ',
            'content24j'=>'本契約は、複数の相対物で締結される場合があり、それぞれが原本を構成し、それらすべてがまと
                        められた場合1つの契約を構成するものとします。 ',
            'content25'=>'9.  Notice.',
            'content25j'=>'通知',
            'content26'=>'Any notice required or otherwise given pursuant to this Agreement shall be in writing and 
                        mailed  certified  return  receipt  requested,  Email,  or  sent  letter  phisically  to  each  parties, 
                        addressed as follows: ',
            'content26j'=>'本契約に従って要求またはその他の方法で用いられる通知は、書面、または要求された証明済みの
                        領収書を郵送するか、電子メールで送信するか、各当事者に物理的に手紙を送付するものとします。',
                        'content27'=>'If to Assignor address:   ',
                        'content28'=>'If to Assignee address:   ',
                        'content27_1'=>'                      '.$em_postal,
                        'content28_1'=>'                      '.$free_postal,
                        'content27_2'=>'                      '.$employer_address,
                        'content28_2'=>'                      '.$freelancer_address,
            'content29'=>'10. Headings.',
            'content29j'=>'見出し',
            'content30'=>'The headings for section herein are for convenience only and shall not affect the meaning of 
                        the provisions of this Agreement.',
            'content30j'=>'本書の項目の見出しは便宜上のものであり、本契約の条項の意味に影響を与えるものではありません。',
            'content31'=>'11. Entire Agreement.',
            'content31j'=>'完全合意 ',
            'content32'=>'This  Agreement  constitutes  the  entire  agreement  between  Assignor  and  Assignee,  and 
                        supersedes any prior understanding or representation of any kind preceding the date of this 
                        Agreement.  There  are  no  other  promises,  conditions,  understandings  or  other  agreements, 
                        whether oral or written, relating to the subject matter of this Agreement.',
            'content32j'=>'本契約は、譲渡人と譲受人の間の完全な合意を構成し、本契約の日付より前のあらゆる種類の
                        事前の理解または表明よりも優先します。  本契約の主題に関連して、口頭または書面を問わず、
                        その他の約束、条件、理解、またはその他の合意はありません。',
            'content33'=>'IN WITNESS WHEREOF, the parties have caused this Agreement to be executed the day and 
                        year first above written.',
            'content33j'=>'その証人として、両当事者は、本契約を上記の最初の年月日で締結させました。',
            'content34'=>'Signature   '.$assignorsign,
            'content35'=>'Print Name  '.$employer_name,
            'content36'=>'Signature   '.$assigneesign,
            'content37'=>'Print Name  '.$freelancer_name,
            ];
        
        $pdf = PDF::loadView('pdfprint.index', $data);  
        return $pdf->download('medium.pdf');
    }
    public function agview()
    {
        return view('viewpdf.index');
    }
}