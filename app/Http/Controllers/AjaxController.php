<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;


class AjaxController extends Controller
{
    //
    public function employersign(Request $request) {
        $employer_sign=$request->em_sign;
        $slug = $request->slug;
        DB::table('jobs')
        ->where('slug', $slug)
        ->update(['employer_sign'=>$employer_sign]);

    }
    public function lancersign(Request $request) {
        $lancer_sign=$request->lan_sign;
        $slug = $request->slug;
        DB::table('jobs')
        ->where('slug', $slug)
        ->update(['lancer_sign'=>$lancer_sign]);

    }
}
