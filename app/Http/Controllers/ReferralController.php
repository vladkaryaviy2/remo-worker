<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Session;
use App\Referral;
use App\Job;
use App\User;
use App\SiteManagement;
use App\Payout;
use Helper;
use Illuminate\Support\Facades\Schema;

class ReferralController extends Controller
{
    public function index()
    {
        if(auth()->user() == null){
            return back();
        }
        else {
            $referrals = Referral::select("users.email","users.first_name","users.last_name",
            "referrals.id as referral_id", "referrals.receive_id","referrals.created_at",
            "referrals.pay_day", "referrals.status as paid_status", "referrals.amount")
            ->leftjoin("users","users.id", "referrals.user_id")
            ->get();
            
            return view('front-end.referral.index')
            ->with('referrals',$referrals);
        }

    }

    public function store(Request $request) {
        $referrals = Referral::where("status", "!=", "Paid")->pluck('id');
        if(count($referrals) > 0)
        {
            foreach($referrals as $temp_ref)
            {
                $new_ref = Referral::find($temp_ref);
                if(!empty($new_ref->referred_job) && ($new_ref->referred_job->paid == 'completed')) {
                    $new_ref->status = "Paid";
                    $new_ref->amount = $request->referral_amount;
                    $new_ref->pay_day = today();
                    $new_ref->save();

                    if($this->add_payout($new_ref->user_id, $request->referral_amount) == 2) {
                        $user = User::find($new_ref->user_id);
                        Session::flash('error', 'Please check the Payapl Id of the User '.$user->first_name." ".$user->last_name);
                        return back();
                    }

                    if($this->add_payout($new_ref->receive_id, $request->referral_amount) == 2) {
                        $user = User::find($new_ref->receive_id);
                        Session::flash('error', 'Please check the Payapl Id of the User '.$user->first_name." ".$user->last_name);
                        return back();
                    }
                }
            }
            Session::flash('message', 'Successfully saved!');
        }else
            Session::flash('error', 'Please select at least 1 project!');

        return back();
    }

    public function status_manage(Request $request) {
        if(!empty($request->item_list) && count($request->item_list) > 0) {
            foreach($item_list = $request->item_list as $item) {
                $new_status = Referral::findOrFail($item);
                if($request->status == 1)
                    $new_status->status = "Enabled";
                else
                    $new_status->status = "Disabled";
                $new_status->save();
            }
            return 1;
        }else {
            return 0;
        }
    }

    public function referral_amount(Request $request) {
        $amount = $request->referral_amount;
        if($amount > 0) {
            $setting = SiteManagement::where('meta_key', 'referral_fee')->first();
            $setting->meta_value = $amount;
            $setting->save();
            return 1;
        }else {
            return 0;
        }
    }

    public function add_payout($user_id, $amount) {
        $payment_settings = SiteManagement::getMetaValue('commision');
        $currency  = !empty($payment_settings) && !empty($payment_settings[0]['currency']) ? $payment_settings[0]['currency'] : 'USD';
        $user = User::find($user_id);
        if ($user->profile->count() > 0) {
            $payout_id = !empty($user->profile->payout_id) ? $user->profile->payout_id : '';
            $payout_detail = !empty($user->profile->payout_settings) ? $user->profile->payout_settings : array();
            
            $payout = new Payout();
            $payout->user()->associate($user_id);
            $payout->amount = $amount;
            $payout->currency = $currency;

            if (!empty($payout_detail)) {
                $payment_details  = Helper::getUnserializeData($user->profile->payout_settings);
                if ($payment_details['type'] == 'paypal') {
                    if (Schema::hasColumn('payouts', 'email')) {
                        $payout->email = $payment_details['paypal_email'];
                    } elseif (Schema::hasColumn('payouts', 'paypal_id')) {
                        $payout->paypal_id = $payment_details['paypal_email'];
                    }
                } else if ($payment_details['type'] == 'bacs') {
                    $payout->bank_details = $user->profile->payout_settings;
                    $payout->paypal_id = 'null';
                } else {
                    $payout->paypal_id = '';
                }
                $payout->payment_method = $payment_details['type'];

            } else if (!empty($payout_id)) {
                $payout->payment_method = 'paypal';
                if (Schema::hasColumn('payouts', 'email')) {
                    $payout->email = $payout_id;
                } elseif (Schema::hasColumn('payouts', 'paypal_id')) {
                    $payout->paypal_id = $payout_id;
                }
            }else {
                
            }
            if(empty($payout->paypal_id)) {
                return 2;
            }else {
                $payout->status = 'pending';
                $payout->order_id = null;
                $payout->projects_ids = null;
                $payout->type = 'job';
                $payout->save();
                return 1;
            }
        }
    }
}
