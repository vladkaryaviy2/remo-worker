<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    protected $fillable = [
        'user_id', 'product_id', 'receive_id','amount', 'pay_day', 'status'
    ];
    // user_id - already existing user who has referral link
    // receive_id - new user registered with referral link
    public function referrer() {
        return $this->belongsTo(User::class, 'receive_id', 'id');
    }

    public function referred_job() {
        return $this->belongsTo(Proposal::class, 'receive_id', 'freelancer_id');
    }
}
