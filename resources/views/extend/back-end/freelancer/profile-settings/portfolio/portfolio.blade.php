<freelancer_portfolio
    :ph_portfolio_title="'{{ trans('lang.ph_portfolio_title') }}'"
    :ph_portfolio_description="'{{ trans('lang.ph_portfolio_description') }}'">
</freelancer_portfolio>