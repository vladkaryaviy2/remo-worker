<div class="wt-dashboardtabs">
    <ul class="wt-tabstitle nav navbar-nav">
        <li class="nav-item">
            <a class="{{{ \Request::route()->getName()==='personalDetail'? 'active': '' }}}" href="{{{ route('personalDetail') }}}">{{{ trans('lang.personal_detail') }}}</a>
        </li>
        <li class="nav-item">
            <a class="{{{ \Request::route()->getName()==='personalVerification'? 'active': '' }}}" href="{{{ route('personalVerification') }}}">{{{ trans('lang.personal_verification') }}}</a>
        </li>
        <li class="nav-item">
            <a class="{{{ \Request::route()->getName()==='freelancerPortfolio'? 'active': '' }}}" href="{{{ route('freelancerPortfolio') }}}">{{{ trans('lang.portfolio') }}}</a>
        </li>
        <li class="nav-item">
            <a class="{{{ \Request::route()->getName()==='projectAwards'? 'active': '' }}}" href="{{{ route('projectAwards') }}}">{{{ trans('lang.project_awards') }}}</a>
        </li>
        <li class="nav-item">
            <a class="{{{ \Request::route()->getName()==='profileCopyright'? 'active': '' }}}" href="{{{ route('profileCopyright') }}}">Copyright</a>
        </li>
    </ul>
</div>
