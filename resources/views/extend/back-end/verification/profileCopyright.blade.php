@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <div class="wt-dbsectionspace wt-haslayout la-ps-freelancer">
        <div class="freelancer-profile" id="user_profile">
            <div class="preloader-section" v-if="loading" v-cloak>
                <div class="preloader-holder">
                    <div class="loader"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                    <div class="wt-dashboardbox wt-dashboardtabsholder">
                        @if (file_exists(resource_path('views/extend/back-end/freelancer/profile-settings/tabs.blade.php'))) 
                            @include('extend.back-end.freelancer.profile-settings.tabs')
                        @else 
                            @include('back-end.freelancer.profile-settings.tabs')
                        @endif
                        <div class="wt-tabscontent tab-content">
                            @if (Session::has('message'))
                                <div class="flash_msg">
                                    <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
                                </div>
                            @endif
                            @if ($errors->any())
                                <ul class="wt-jobalerts">
                                    @foreach ($errors->all() as $error)
                                        <div class="flash_msg">
                                            <flash_messages :message_class="'danger'" :time ='10' :message="'{{{ $error }}}'" v-cloak></flash_messages>
                                        </div>
                                    @endforeach
                                </ul>
                            @endif
                            <div class="wt-personalskillshold tab-pane active fade show" id="wt-skills">
                                {!! Form::open(['url' => url('employer/store-profile-settings'), 'class' =>'wt-userform', 'id' => 'submitCopyright', '@submit.prevent' => 'submitCopyright']) !!}
                                    <div class="wt-yourdetails wt-tabsinfo">
                                        <div class="wt-tabscontenttitle">
                                            <h2>{{{ trans('lang.your_details') }}}</h2>
                                        </div>
                                        <div class="lara-detail-form">
                                            <fieldset>
                                                <div class="form-group form-group-half">
                                                    {!! Form::text( 'first_name', e(Auth::user()->first_name), ['class' =>'form-control', 'placeholder' => trans('lang.ph_first_name')] ) !!}
                                                </div>
                                                <div class="form-group form-group-half">
                                                    {!! Form::text( 'last_name', e(Auth::user()->last_name), ['class' =>'form-control', 'placeholder' => trans('lang.ph_last_name')] ) !!}
                                                </div>
                                                <div class="form-group">
                                                </div>
                                                <div class="form-group">
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                    
                                    <div class="wt-location wt-tabsinfo">
                                        <div class="wt-tabscontenttitle">
                                            <h2>{{ trans('lang.your_loc') }}</h2>
                                        </div>
                                        <div class="wt-formtheme">
                                            <fieldset>
                                                <div class="form-group form-group-half">
                                                    <span class="wt-select">
                                                        {!! Form::select('location', $locations, Auth::user()->location_id ,array('class' => '', 'placeholder' => trans('lang.ph_select_location'))) !!}
                                                    </span>
                                                </div>
                                                <div class="form-group form-group-half">
                                                    {!! Form::text( 'address', e($address), ['id'=>"pac-input", 'class' =>'form-control', 'placeholder' => trans('lang.ph_your_address')] ) !!}
                                                </div>
                                                <div class="form-group form-group-half">
                                                    {!! Form::text( 'longitude', e($longitude), ['id'=>"lng-input", 'class' =>'form-control', 'placeholder' => trans('lang.ph_enter_logitude')]) !!}
                                                </div>
                                                <div class="form-group form-group-half">
                                                    {!! Form::text( 'latitude', e($latitude), ['id'=>"lat-input", 'class' =>'form-control', 'placeholder' => trans('lang.ph_enter_latitude')]) !!}
                                                </div>
                                                <div class="form-group form-group-half">
                                                    {!! Form::text( 'postal', e($postal), ['id'=>"postal-input", 'class' =>'form-control', 'placeholder' => 'Postal Code']) !!}
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="wt-updatall">
                                        <i class="ti-announcement"></i>
                                        <span>{{{ trans('lang.save_changes_note') }}}</span>
                                        {!! Form::submit(trans('lang.btn_save_update'), ['class' => 'wt-btn', 'id'=>'submit-profile']) !!}
                                    </div>
                                {!! form::close(); !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
