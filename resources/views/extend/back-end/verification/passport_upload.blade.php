<div class="wt-location wt-tabsinfo">
    <div class="wt-tabscontenttitle">
        <h2>{{{ trans('lang.personal_verification') }}}</h2>
    </div>
    <div class="wt-settingscontent">
        @if (!empty($profile->passport))
            @php $image = '/uploads/users/'.Auth::user()->id.'/verification/'.$profile->passport; @endphp
            <div class="wt-formtheme wt-userform">
                <div v-if="this.uploaded_banner">
                    <upload-image 
                        :id="'banner_id'" 
                        :img_ref="'banner_ref'" 
                        :url="'{{url('user/upload-temp-image')}}'"
                        :name="'hidden_verification_image'"
                        >
                    </upload-image>
                </div>
                <div class="wt-uploadingbox" v-else>
                    <figure><img src="{{{asset($image)}}}" alt="{{{ trans('lang.profile_photo') }}}"></figure>
                    <div class="wt-uploadingbar">
                        <div class="dz-filename">{{{$profile->passport}}}</div>
                        <em>{{{ trans('lang.file_size') }}}<a href="javascript:void(0);" class="lnr lnr-cross" v-on:click.prevent="removeBanner('hidden_banner')"></a></em>
                    </div>
                </div>
                <input type="hidden" name="hidden_verification_image" id="hidden_banner" value="{{{$profile->passport}}}"> 
            </div>
        @else
            <div class="wt-formtheme wt-userform">
                <upload-image 
                    :id="'banner_id'" 
                    :img_ref="'banner_ref'" 
                    :url="'{{url('freelancer/upload-temp-image')}}'"
                    :name="'hidden_verification_image'"
                    >
                </upload-image>
                <input type="hidden" name="hidden_verification_image" id="hidden_banner"> 
            </div>
        @endif
    </div>
</div>
