@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    <div class="wt-dbsectionspace wt-haslayout la-ps-freelancer">
        <div class="freelancer-profile" id="user_profile">
            <div class="preloader-section" v-if="loading" v-cloak>
                <div class="preloader-holder">
                    <div class="loader"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                    <div class="wt-dashboardbox wt-dashboardtabsholder">
                        @if (file_exists(resource_path('views/extend/back-end/freelancer/profile-settings/tabs.blade.php'))) 
                            @include('extend.back-end.freelancer.profile-settings.tabs')
                        @else 
                            @include('back-end.freelancer.profile-settings.tabs')
                        @endif
                        <div class="wt-tabscontent tab-content">
                            @if (Session::has('message'))
                                <div class="flash_msg">
                                    <flash_messages :message_class="'success'" :time ='5' :message="'{{{ Session::get('message') }}}'" v-cloak></flash_messages>
                                </div>
                            @endif
                            @if ($errors->any())
                                <ul class="wt-jobalerts">
                                    @foreach ($errors->all() as $error)
                                        <div class="flash_msg">
                                            <flash_messages :message_class="'danger'" :time ='10' :message="'{{{ $error }}}'" v-cloak></flash_messages>
                                        </div>
                                    @endforeach
                                </ul>
                            @endif
                            <div class="wt-personalskillshold tab-pane active fade show" id="wt-skills">
                                {!! Form::open(['url' => '', 'class' =>'wt-userform', 'id' => 'freelancer_verification', '@submit.prevent'=>'submitVerification']) !!}
                                    @if (empty($verified) || $verified == 0 || empty($profile->passport))
                                    <div class="wt-bannerphoto wt-tabsinfo">
                                        @if (file_exists(resource_path('views/extend/back-end/verification/passport_upload.blade.php'))) 
                                            @include('extend.back-end.verification.passport_upload')
                                        @else 
                                            @include('back-end.verification.passport_upload')
                                        @endif    
                                    </div>
                                    <div class="wt-updatall">
                                        <i class="ti-announcement"></i>
                                        <span>{{{ trans('lang.save_changes_note') }}}</span>
                                        {!! Form::submit(trans('lang.btn_save_update'), ['class' => 'wt-btn', 'id'=>'submit-profile']) !!}
                                    </div>
                                    @else
                                    @php $passport_path = '/uploads/users/'.Auth::user()->id.'/verification/'.$profile->passport; @endphp
                                    <div class="wt-location wt-tabsinfo">
                                        <div class="wt-tabscontenttitle">
                                            <h2>{{{ trans('lang.personal_verification') }}}</h2>
                                        </div>
                                        <div class="wt-settingscontent">
                                            <div class="wt-formtheme wt-userform">
                                                <img src="{{$passport_path}}" style="width: 100%; height: auto; border: 10px solid #dddddd88;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wt-updatall">
                                        <i class="ti-announcement"></i>
                                        <span class="text-info mb-3">You are already verified.</span>
                                    </div>
                                    @endif
                                {!! form::close(); !!}  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
