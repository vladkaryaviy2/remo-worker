<div class="wt-dashboardtabs">
    <ul class="wt-tabstitle nav navbar-nav">
        <li class="nav-item">
            <a class="{{{ \Request::route()->getName()==='employerPersonalDetail'? 'active': '' }}}" href="{{{ route('employerPersonalDetail') }}}">{{{ trans('lang.profile_detail') }}}</a>
        </li>
        <li class="nav-item">
            <a class="{{{ \Request::route()->getName()==='personalVerification'? 'active': '' }}}" href="{{{ route('personalVerification') }}}">{{{ trans('lang.personal_verification') }}}</a>
        </li>
        <li class="nav-item">
            <a class="{{{ \Request::route()->getName()==='profileCopyright'? 'active': '' }}}" 
            href="{{{ route('profileCopyright') }}}">Copyright</a>
        </li>
    </ul>
</div>