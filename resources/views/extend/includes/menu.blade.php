{{-- {{dd($page_order)}} --}}
<nav id="wt-nav" class="wt-nav navbar-expand-lg">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <i class="lnr lnr-menu"></i>
    </button>
    <div class="collapse navbar-collapse wt-navigation" id="navbarNav">
        <ul class="navbar-nav">
            @php
                $inner_page  = App\SiteManagement::getMetaValue('inner_page_data');
                $add_f_navbar = !empty($inner_page) && !empty($inner_page[0]['add_f_navbar']) ? $inner_page[0]['add_f_navbar'] : '';
                $add_emp_navbar = !empty($inner_page) && !empty($inner_page[0]['add_emp_navbar']) ? $inner_page[0]['add_emp_navbar'] : '';
                $add_job_navbar = !empty($inner_page) && !empty($inner_page[0]['add_job_navbar']) ? $inner_page[0]['add_job_navbar'] : '';
                $add_service_navbar = !empty($inner_page) && !empty($inner_page[0]['add_service_navbar']) ? $inner_page[0]['add_service_navbar'] : '';
                $add_article_navbar = !empty($inner_page) && !empty($inner_page[0]['add_article_navbar']) ? $inner_page[0]['add_article_navbar'] : '';
                $menu_settings  = App\SiteManagement::getMetaValue('menu_settings');
                $freelancer_order = !empty($menu_settings['pages']) ? Helper::getArrayIndex($menu_settings['pages'], 'id', 'freelancers') : ""; 
                $employer_order = !empty($menu_settings['pages']) ? Helper::getArrayIndex($menu_settings['pages'], 'id', 'employers') : ''; 
                $job_order = !empty($menu_settings['pages']) ? Helper::getArrayIndex($menu_settings['pages'], 'id', 'jobs') : ''; 
                $service_order = !empty($menu_settings['pages']) ? Helper::getArrayIndex($menu_settings['pages'], 'id', 'services') : ''; 
                $article_order = !empty($menu_settings['pages']) ? Helper::getArrayIndex($menu_settings['pages'], 'id', 'articles') : ''; 
            @endphp
            @if ($add_f_navbar !== 'false')
                <li style="{{!empty($freelancer_order) ? 'order:'.$freelancer_order : 'order:99' }}">
                    <a href="{{url('search-results?type=freelancer')}}">
                        {{{ trans('lang.view_freelancers') }}}
                    </a>
                </li>
            @endif
            @if ($add_emp_navbar !== 'false')
            <li style="{{!empty($employer_order) ? 'order:'.$employer_order : 'order:99' }}">
                <a href="{{url('search-results?type=employer')}}">
                    {{{ trans('lang.view_employers') }}}
                </a>
            </li>
            @endif
            @if ($add_job_navbar !== 'false')
                @if ($type =='jobs' || $type == 'both')
                    <li style="{{!empty($job_order) ? 'order:'.$job_order : 'order:99' }}">
                        <a href="{{url('search-results?type=job')}}">
                            {{{ trans('lang.browse_jobs') }}}
                        </a>
                    </li>
                @endif
            @endif
            @php 
                $order=''; $page_order=''; 
                $custom_menus = !empty($menu_settings['custom_links']) ? $menu_settings['custom_links'] : '';
                // dd($custom_menus);
            @endphp
            @if (!empty($custom_menus))
                @foreach($custom_menus as $custom_key => $custom_value)
                    @if ($custom_value['relation_type'] == 'parent')
                        @php 
                            $order = Helper::getCustomMenuPageOrder($custom_value['custom_slug']);
                        @endphp
                        <li style="{{!empty($order) ? 'order:'.$order : 'order:99' }}">
                            <a href="{{ empty($custom_value['custom_link']) || $custom_value['custom_link'] == '#' ? 'javascript:void(0)' : $custom_value['custom_link'] }}">
                                {{$custom_value['custom_title']}}
                            </a>
                            @php 
                            $custom_menu_child = Helper::getCustomMenuChild($custom_value['custom_slug']);
                            @endphp
                            @if (!empty($custom_menu_child))
                                <ul class="sub-menu">
                                    @foreach($custom_menu_child as $custom_child)
                                        @if (!empty($custom_child) && !empty($custom_child['type']) && $custom_child['type'] == 'custom_menu')
                                            <li>
                                                <a href="{{empty($custom_child['link']) || $custom_child['link'] == '#' ? 'javascript:void(0)' : $custom_child['link']}}">
                                                    {{{$custom_child['title']}}}
                                                </a>
                                            </li>
                                        @elseif (!empty($custom_child)) 
                                            <li class="@if ($pageID == $custom_child['slug'] ) current-menu-item @endif">
                                                <a href="{{url('page/'.$custom_child['slug'].'/')}}">
                                                    {{{$custom_child['title']}}}
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endif
                @endforeach
            @endif
        </ul>
    </div>
</nav>