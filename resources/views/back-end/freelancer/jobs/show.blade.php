@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('content')
    @php
        $verified_user = \App\User::select('user_verified')
        ->where('id', $job->employer->id)->pluck('user_verified')->first();
    @endphp
    <section class="wt-haslayout wt-dbsectionspace la-dbproposal" id="jobs">
        @if (Session::has('error'))
            <div class="flash_msg">
                <flash_messages :message_class="'danger'" :time='5' :message="'{{{ Session::get('error') }}}'" v-cloak></flash_messages>
            </div>
        @endif
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                @if ($proposal->status == "cancelled" && !empty($cancel_reason))
                    <div class="wt-jobalertsholder">
                        <ul class="wt-jobalerts">
                            <li class="alert alert-danger alert-dismissible fade show">
                                <span><em>{{ trans('lang.sorry') }}</em> {{ trans('lang.job_cancelled') }}</span>
                                <a href="javascript:void(0)" class="wt-alertbtn danger" v-on:click.prevent="viewReason('{{$cancel_reason->description}}')" >{{ trans('lang.reason') }}</a>
                                <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="Close"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                    </div>
                @endif
                <div class="wt-dashboardbox">
                    <div class="wt-dashboardboxtitle">
                        <h2>{{ trans('lang.job_dtl') }}</h2>
                    </div>
                    <div class="wt-dashboardboxcontent wt-jobdetailsholder">
                        <div class="wt-freelancerholder wt-tabsinfo">
                            <div class="wt-jobdetailscontent">
                                <div class="wt-userlistinghold wt-featured wt-userlistingvtwo">
                                    @if (!empty($job->is_featured) && $job->is_featured === 'true')
                                        <span class="wt-featuredtag">
                                            <img src="{{{ asset('images/featured.png') }}}" alt="{{ trans('lang.is_featured') }}"
                                                data-tipso="Plus Member" class="template-content tipso_style">
                                        </span>
                                    @endif
                                    <div class="wt-userlistingcontent">
                                        <div class="wt-contenthead">
                                            @if (!empty($employer_name) || !empty($job->title) )
                                                <div class="wt-title">
                                                    @if (!empty($employer_name))
                                                        <a href="{{{ url('profile/'.$job->employer->slug) }}}">
                                                            @if($verified_user === 1)
                                                                <i class="fa fa-check-circle"></i>&nbsp;
                                                            @endif
                                                            {{{ $employer_name }}}
                                                        </a>
                                                    @endif
                                                    @if (!empty($job->title))
                                                        <h2>{{{ $job->title }}}</h2>
                                                    @endif
                                                </div>
                                            @endif
                                            <ul class="wt-userlisting-breadcrumb">
                                                @if (!empty($job->price))
                                                    <li><span><i class="far fa-money-bill-alt"></i> {{ !empty($symbol) ? $symbol['symbol'] : '$' }}{{{ $job->price }}}</span></li>
                                                @endif
                                                @if (!empty($job->location->title))
                                                    <li>
                                                        <span>
                                                            <img src="{{{asset(Helper::getLocationFlag($job->location->flag))}}}"
                                                            alt="{{ trans('lang.img') }}"> {{{ $job->location->title }}}
                                                        </span>
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                        <div class="wt-rightarea">
                                            <div class="wt-hireduserstatus">
                                                <figure><img src="{{{ asset($employer_image) }}}" alt="{{ trans('lang.profie_img') }}"></figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="white night">
                            <div class="wt-rightarea wt-titlewithsearch">
                                @if ($job->status === 'hired' && Auth::user()->getRoleNames()->first() == 'employer')
                                    <form class="wt-formtheme wt-formsearch" id="change_job_status">
                                        <fieldset>
                                            <div class="form-group">
                                                <span class="wt-select">
                                                    {!! Form::select('status', $project_status, $job->status, array('id' =>'job_status', 'data-placeholder' => trans('lang.select_status'), '@change' => 'jobStatus('.$job->id.', '.$accepted_proposal->id.', "'.$cancel_proposal_text.'", "'.$cancel_proposal_button.'", "'.$validation_error_text.'", "'.$cancel_popup_title.'")')) !!}
                                                </span>
                                                <a href="javascrip:void(0);" class="wt-searchgbtn job_status_popup" @click.prevent='jobStatus({{$job->id}}, {{$accepted_proposal->id}}, "{{$cancel_proposal_text}}", "{{$cancel_proposal_button}}", "{{$validation_error_text}}", "{{$cancel_popup_title}}")'><i class="fa fa-check"></i></a>
                                            </div>
                                        </fieldset>
                                    </form>
                                @endif
                                <div class="wt-hireduserstatus">
                                    <h5>{{ !empty($symbol) ? $symbol['symbol'] : '$' }}{{{ $accepted_proposal->amount }}}</h5>
                                    @if (!empty($completion_time))
                                        <span>{{{ $completion_time }}}</span>
                                    @endif
                                </div>
                                <div class="wt-hireduserstatus">
                                    <i class="far fa-envelope"></i>
                                    <a href="javascript:void(0);"  v-on:click.prevent="showCoverLetter('{{ $accepted_proposal->id }}')"  ><span>{{ trans('lang.cover_letter') }}</span></a>
                                </div>
                                @if (!empty($attachments))
                                <div class="wt-hireduserstatus">
                                    <i class="fa fa-paperclip"></i>
                                    <?php $count = 0;?>

                                    {!! Form::open(['url' => url('proposal/download-attachments'), 'class' =>'post-job-form wt-haslayout', 'id' => 'download-attachments-form-'.$accepted_proposal->freelancer_id]) !!}
                                        @foreach ($attachments as $attachment)
                                            @if (Storage::disk('local')->exists('uploads/proposals/'.$accepted_proposal->freelancer_id.'/'.$attachment))
                                                {!! Form::hidden('attachments['.$count.']', $attachment, []) !!}
                                                @php $count++; @endphp
                                            @endif
                                        @endforeach
                                        {!! Form::hidden('freelancer_id', $accepted_proposal->freelancer_id, []) !!}
                                    {!! form::close(); !!}
                                    <a href="javascript:void(0);" v-on:click.prevent="downloadAttachments('{{'download-attachments-form-'.$accepted_proposal->freelancer_id}}')" ><span>0 file attached</span></a>
                                </div>
                                @endif
                                <div class="wt-hireduserstatus">
                                    <i class="fa fa-sign-language"></i>
                                    <a href="javascript:void(0);"  v-on:click.prevent="copyright_show=!copyright_show"  ><span>Asignment</span></a>
                                </div>
                            </div>
                            <div class="wt-userlistinghold wt-featured wt-proposalitem" v-if="copyright_show">
                                <div class="wt-tabscontenttitle">
                                    <h2>{{ trans('lang.copyright_transfer_agreement') }}</h2>
                                </div>
                                
                                <form class="white night" action="{{ route('agreementprintpdf', $slug=$job->slug) }}" method="get">
                                {!! csrf_field() !!}
                                    <h1>Please fill the gaps with agreement.</h1>
                                    @if (empty($employer_sign) || empty($lancer_sign))
                                    <div class="checkbox">
                                    <label style="text-align:center"><input type="checkbox">     I read agreement and swear no any complaings about agreement details.</label>
                                    </div>
                                    <p style="text-align:center">Read <a href="{{ route('agreementview', $slug=$job->slug) }}">Agreement</a></p>
                                    <div class="row" style="height:200px">
                                        <div class="col-sm-6" style="float:left; width:350px; height:100px; text-align:center">
                                        <h5>Asignee signature</h5>
                                        @if($esign_state)
                                        <br>
                                        <h3>Signed</h3>
                                        @endif
                                        @if(!$esign_state)
                                        <br>
                                       <h3>Signature Pending</h3>
                                        @endif
                                        </div>
                                        <div class="col-sm-6"  style="float:right; width:350px; height:100px; text-align:center">
                                        <h5>Asignor signature</h5>
                                        @if($fsign_state)
                                        <br>
                                        <h3>Signed</h3>
                                        @endif
                                        @if(!$fsign_state)
                                        <input type="text" style="width:100%" name="assigneesign" id="assigneesign" required>
                                        @endif
                                        </div>
                                    </div>
                                    @endif
                                    @if(!empty($employer_sign) && !empty($lancer_sign))
                                    <br>
                                    <br>
                                    <h4 style="text-align:center">Aleady signed by both parties</h4>
                                    <br>
                                    <br>
                                    <button type="submit" class="row btn btn-danger" style="width:200px; display:block; margin-left:auto; margin-right:auto;"><h4 style="color:white">DOWNLOAD</h4></button>
                                    @endif
                                </form>
                                
                                @if (empty($lancer_sign))
                                <button id="agreement" onclick="sav('{{$aaa}}')" class="row btn btn-danger" style="width:200px; display:block; margin-left:auto; margin-right:auto;">Save</button>
                                @endif
                            </div>
                       
                        <div class="wt-projecthistory">
                            <div class="wt-tabscontenttitle">
                                <h2>{{ trans('lang.project_history') }}</h2>
                            </div>
                            <div class="wt-historycontent">
                                <private-message :ph_job_dtl="'{{ trans('lang.ph_job_dtl') }}'" :upload_tmp_url="'{{url('proposal/upload-temp-image')}}'" :id="'{{$proposal->id}}'" :recipent_id="'{{$job->user_id}}'"></private-message>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
            </div>
        </div>
    </section>
    <script>
		function sav(e){
			var lancer_sign = document.getElementById("assigneesign").value;
			var data={'lan_sign': lancer_sign,'slug':e};
			$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
			});

			$.ajax({
				type: 'post',
				url: '/lancersign',
				data: data,
				success: function (data) {}
            });

            location.reload();
            document.getElementById("agreement").style.display="none";
		}
	</script>
@endsection
