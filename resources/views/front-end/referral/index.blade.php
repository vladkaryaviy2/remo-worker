@extends(file_exists(resource_path('views/extend/back-end/master.blade.php')) ? 'extend.back-end.master' : 'back-end.master')
@section('referral_style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.bootstrap4.min.css"> -->
<style>
/* #referral_page .dt-buttons.btn-group.flex-wrap, .dataTables_filter{
    float:right;
} */
/* #referral_page .dataTables_length {
    width: 200px;
    float: left;
} */
/* #referral_page .dt-buttons.btn-group.flex-wrap {
    padding: 5px;
} */
#amount_field * {
    padding: 10px;
}
.paginate_button {
    list-style: none;
}
#referral_filter_btn {
    margin: 10px;
}
.card, .card-body {
    background: white;
}
.card-header {
    background-color: unset !important;
    border-bottom: none !important;
}
#referral_table tbody tr {
    cursor:pointer;
}
</style>
<link rel="stylesheet" href="{{asset('css/toggle-style.css')}}">
@stop
@section('content')
<div class="card" id="referral_page">
        @if($errors->any())
        <div class="flash_msg">
                <div class="wt-jobalertsholder la-email-warning float-right"><ul id="wt-jobalerts">
                <li class="alert alert-alert alert-dismissible fade show"><span>
                    {{ implode('', $errors->all('message')) }}
                </span>
                <a href="javascript:void(0)" data-dismiss="alert" aria-label="Close" class="close">
                <i class="fa fa-close"></i></a></li></ul>
                </div>
        @endif
        @if (Session::has('message'))
            <div class="flash_msg">
                <div class="wt-jobalertsholder la-email-warning float-right"><ul id="wt-jobalerts">
                <li class="alert alert-success alert-dismissible fade show"><span>
                {{ Session::get('message') }}
                </span>
                <a href="javascript:void(0)" data-dismiss="alert" aria-label="Close" class="close">
                <i class="fa fa-close"></i></a></li></ul>
                </div>
            </div>
        @elseif (Session::has('error'))
            <div class="flash_msg">
                <div class="wt-jobalertsholder la-email-warning float-right"><ul id="wt-jobalerts">
                <li class="alert alert-danger alert-dismissible fade show"><span>{{ Session::get('error') }}</span>
                <a href="javascript:void(0)" data-dismiss="alert" aria-label="Close" class="close">
                <i class="fa fa-close"></i></a></li></ul>
                </div>
            </div>
        @endif
    <form name="referral_form" action="{{route('referral_save')}}" method="POST">
        @csrf
        <div class="card-header row">
            <div class="col-md-3">
                <h3 class="card-title" style = "">Referral Program</h3>
            </div>
            <div class="col-md-3">
                    <span class="switch">
                    <label></label>
                        <input type="checkbox" class="switch" id="switch-id" checked>
                        <label for="switch-id"></label>
                    </span>
                    <div class="contentA">
                    Disabled
                    </div>
                    <div class="contentB">Enabled</div>
            </div>
            <div class="col-md-6" id="amount_field">
                <span class="card-title" style = "">Referral Amount</span>
                <input type="number" name="referral_amount" id="referral_amount" value="{{ \App\SiteManagement::getPlainMeta('referral_fee') }}" min="5" required>
                <span class="card-title" style = "">USD</span>
                <a class="btn btn-success" id="referral_amount_save">save</a>
            </div>
        </div>
        <div class="card-body">
            <h2 class="card-title" style = "text-align:center;">Referral List</h2>
            <div class="row" id="referral_filter_btn">
                <div class="col-md-2">
                </div>
                <div class="col-md-2">
                    <span class="btn btn-md btn-warning status_filter btn-block" id="Pending">Pending</span>
                </div>
                <div class="col-md-2">
                    <span class="btn btn-md btn-success status_filter btn-block" id="Successed">Successed</span>
                </div>
                <div class="col-md-2">
                    <span class="btn btn-md btn-primary status_filter btn-block" id="Paid">Paid</span>
                </div>
                <div class="col-md-2" id="csv_export"></div>
            </div>
            <div class="table-responsive">
                <table class="table color-table info-table display" id="referral_table" style="width:100%">
                    <thead class="hidden">
                        <tr>
                            <th>
                                <!-- <input name="total" value="" id="" type="checkbox">  -->
                            </th>
                            <th>Date</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Referer</th>
                            <th>Job Title</th>
                            <th>Amount</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($referrals as $temp)
                        <tr>
                            <td>
                                @if($temp->paid_status != "Paid")
                                <span class="rf-checkbox">
                                <input class="referral_id_list" name="referral_id[]" value="{{$temp->referral_id}}" id="rf-check-{{$temp->referral_id}}" type="checkbox"> 
                                <label for="rf-check-{{$temp->id}}"></label>
                                </span>
                                @endif
                            </td>
                            <td> {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $temp->created_at)->format('d / M / Y') }} </td>
                            <td>{{$temp->first_name.' '.$temp->last_name}}</td>
                            <td>{{$temp->email}}</td>
                            <td>{{$temp->referrer->first_name.' '.$temp->referrer->last_name}}</td>
                            <td><a>{{ !empty($temp->referred_job) ? \App\Job::getJobDetail($temp->referred_job->job_id)->title:''}}</a></td>
                            <td>
                                @if($temp->amount && $temp->pay_day)
                                {{'$'.$temp->amount." (".$temp->pay_day.")"}}
                                @endif
                            </td>
                            <td>
                            @if($temp->paid_status == "Paid")
                            <span class="badge badge-primary">Paid</span>
                            @elseif($temp->paid_status == "Disabled")
                                <span class="badge badge-secondary">Disabled</span>
                            @else
                                @if(!empty($temp->referred_job) && $temp->referred_job->paid == "pending")
                                <span class="badge badge-warning">Pending</span>
                                @elseif(!empty($temp->referred_job) && $temp->referred_job->paid == "completed")
                                <span class="badge badge-success">Completed</span>
                                @else
                                <span class="badge badge-warning">Pending</span>
                                @endif
                            @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-12 text-center">
                <button class="btn btn-success btn-md rounded d-none" id="btn_paid" onclick="if(confirm('are you sure?')) $('').submit()">
                Go to paid
                </button>
            </div>
        </div>
    </form>
</div>
@endsection
@section('referral_script')
<!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.bootstrap4.min.js"></script>
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> -->
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script> -->
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> -->
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<!-- <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script> -->
<!-- <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script> -->
<!-- <script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.7/js/dataTables.fixedHeader.min.js"></script> -->

<script>
$(document).ready(function() {
    var table = $('#referral_table').DataTable( {
        dom: 'Blfrtip',
		responsive: true,
        buttons: [ 
            {
                extend: 'csv',
                text: 'CSV Export',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                }
            },
        ],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        // columnDefs: [ {
        //     orderable: false,
        //     className: 'select-checkbox',
        //     targets:   0
        // } ],
        // select: {
        //     style:    'os',
        //     selector: 'td:first-child'
        // },
        // order: [[ 1, 'asc' ]]
    } );
    table.buttons().container()
        .appendTo( '#csv_export' );
} );
</script>
<script>
$(document).on('click', '.status_filter', function() {
    var status = $(this).attr('id')
    console.log(status, " status", $(this).attr('id'))
    $("input[type='search']").val(status)
    $("input[type='search']").keyup()
})

$(document).on('click', '#referral_amount_save', function() {
    var amount = $("#referral_amount").val()
    $.ajax({
        method:"Post",
        url:"/admin/referral_amount",
        data:{referral_amount:amount},
        success: function(res) {
            // if(res == 1)
            //     window.location.reload()
        }
    })
})
$(function () {

    $("#switch-id").change(function () {
        var status = 0;
        if ($(this).is(":checked")) {
            $(".contentB").show();
            $(".contentA").hide();
            status = 1;
        } else {
            $(".contentB").hide();
            $(".contentA").show();
            status = 0;
        }
        var checked_list = [];
        var list = $('.referral_id_list')
        list.map(function(i, item){
            if($('#'+item.id).is(":checked")) {
                checked_list.push(item.value)
            }
        })

        $.ajax({
            method:"post",
            url:"/admin/status_manage",
            data:{status:status, item_list:checked_list},
            success: function(res) {
                if(res == 1)
                    window.location.reload()
            }
        })
    });
});

$('#referral_table tbody').on( 'click', 'tr', function () {
    let status = $(this).find('td').eq(7).text()
    console.log(status.replace(/\s/g, ''), "status")
    if(status.replace(/\s/g, '') == "Paid")
        $("#btn_paid").addClass('d-none')
    else
        $("#btn_paid").removeClass('d-none')

    // if ( $(this).hasClass('selected') ) {
    //     $(this).removeClass('selected');
    // }
    // else {
    //     table.$('tr.selected').removeClass('selected');
    //     $(this).addClass('selected');
    // }
} );
</script>
@stop