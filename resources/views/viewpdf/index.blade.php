<!DOCTYPE html>
<html>
    <head>
        <style>
            body {
                font-family: ipag;
            }
        </style>
    </head>
    <body>
        <h3 style="text-align:center">ASSIGNMENT AGREEMENT FOR COPYRIGHTED VIDEO
            <br>著作権動画の割り当て契約
        </h3>
        <br>
        <br>THIS ASSIGNMENT is made this________________day of__________, 20____, (the “Effective date”) by and between_______________(the “Assignor”) and__________________(the “Assignee”) (collectively, the “Parties”).
        <br>この割り当ては、 20XX年XX月XX日に行われます（「有効日付」）____________（「 譲渡人」）および__________（「譲受人」）（総称して「当事者」）の契約。
        <br>
        <br>WHEREAS, Assignor is the copyright holder and owner of all proprietary interest in______________(describe video)____________(the “Work”).
        <br>譲渡人は著作権所有者であり、譲受人は________________[プロジェクト名]（「事業」）での成果物の譲受人である。
        <br>
        <br>AND WHEREAS, Assignor wishes to transfer all rights, ownership and interest in the finish product of Work which Assignor delivered, including the copyright and all other intellectual property rights, to Assignee, under the terms set forth in this Agreement;
        <br>譲渡人は、本契約に定められた条件に基づいて、著作権およびその他すべての知的財産権を含め、譲渡人が提供した作品の完成品に対するすべての権利、所有権、および利益を譲受人に譲渡することを希望します。
        <br>
        <br>NOW THEREFORE, in consideration of the mutual promises, covenants, warranties, and other good and valuable consideration set forth herein, the Parties hereby agree as follows:
        <br>そのため、相互の約束、契約、保証、および本書に記載されているその他の有益で価値のある考慮事項を考慮して、両当事者は以下のことに同意します。
        <br>
        <br>1. Assignment of the Work.
        <br>Assignor hereby irrevocably assigns to Assignee all rights, title and interest to the Work, including all copyright ownership and interest, and all moral rights associated with the creation of the Work and all other intellectual property associated with the Work. Assignee shall be the exclusive owner of the Work and of the copyright in the Work from the Effective Date forward, and shall have the exclusive right to secure registration of the copyright in the Work with the U.S. Copyright Office and shall have the exclusive right to secure registration of the copyright internationally. No rights in the Work, or in the copyright in the Work, shall be retained by Assignor, nor shall there be any reversion of those rights to Assignor in the future.
        <br>作業の割り当て
        <br>譲渡人は、これにより、著作権の所有権と利益、および著作物の作成に関連するすべての著作者人格権、および著作物に関連するその他すべての知的財産を含む、著作物に対するすべての権利、権原、および利益を譲受人に取消不能に譲渡します。 譲受人は、発効日以降、著作物および著作物の著作権の独占的所有者であり、米国著作権局への著作物の著作権の登録を確保する独占的権利を有し、確保する独占的権利を有するものとします。国際的な著作権の登録。 作品の権利、または作品の著作権は、譲渡人によって保持されないものとします。また、将来、これらの権利が譲渡人に返還されることもありません。
        <br>
        <br>2. Extend of copyright protection.
        <br>Works produced during the course of the project are not included in the agreement of the contract. In addition, this contract does not permit the use of works in the production process or the exercise of copyrights for products other than the delivered products.
        <br>著作権の及ぶ範囲
        <br>プロジェクトの過程で製作された著作物は契約書の同意に含まれません。また、納品された製作物以外に対する製作過程の著作物の使用、著作権の行使をこの契約書では認めません。
        <br>
        <br>3. Payment.
        <br>In consideration of the assignment made by Assignor, as well as Assignor’s promises, representations, covenants and warranties under this Agreement, upon execution of this Agreement Assignee shall pay to Assignor the amount of $XXX.00 which both parties agreed at project on domain.com.
        <br>支払い
        <br>譲渡人による譲渡、および本契約に基づく譲渡人の約束、表明、誓約および保証を考慮して、譲受人は、本契約の締結時に、両当事者がドメインのプロジェクトで合意したXXX.00ドルを譲渡人に支払うものとします。 remoworker.com
        <br>
        <br>4. Assignor’s Representations and Warranties.
        <br>Assignor hereby represents and warrants the following:
        <br>譲渡人の表明および保証
        <br>譲渡人は、これにより以下を表明および保証します。
        <br>
        <br>a. Assignor has the legal authority to grant the assignment of the Work, including all copyright rights and proprietary interest therein, as set forth in Section 1. No other person or entity is required to consent to this assignment or to this Agreement for it to be valid and complete.
        <br>譲渡人は、１項に記載されているように、すべての著作権および所有権を含む作品の譲渡を許可する法的権限を有します。他の個人または団体は、この譲渡または本契約が有効であることに同意する必要はありません。 この契約書が完全な同意を意味し、保証します。
        <br>
        <br>b. There are currently no licenses outstanding granting any other person or entity the right to enjoy or lay claim to any copyright rights or privileges in the Work, other than those licenses listed in Appendix A, copies of which are attached as Exhibits. Assignor will not attempt to grant any such licenses at any time in the future. To the best of Assignor’s knowledge, the Work, and all copyright interest in the Work, is free and clear of any liens, security interests, or other encumbrances.
        <br>現在、付録Aに記載されているライセンス以外に、著作物の著作権または特権を享受または主張する権利を他の個人または団体に付与している不透明なライセンスはありません。これらのライセンスのコピーは別紙 として添付されています。 譲渡人は、将来いつまでもライセンスに関する権利を主張しようとはしません。 譲渡人の知る限り、著作物、および著作物のすべての著作権上の利益は、先取特権、担保権、またはその他 の負担はなく虚偽もありません。
        <br>
        <br>c. To the best of Assignor’s knowledge, the Work does not infringe upon the rights, copyright or otherwise, of any other person or entity.
        <br>譲渡人の知る限り、著作物は他の個人または団体の権利、著作権、またはその他を侵害するものではありません。
        <br>
        <br>d. There are no claims currently pending or threatened, nor does Assignor have any reason to believe that any claims will be brought or threatened in the future, against Assignor’s right, ownership or interest in the Work.
        <br>現在紛争中または脅迫されている請求はなく、譲渡人は、著作物に対する譲渡人の権利、所有権、または利益に対して、将来、請求が提起または脅迫されるとことはないと誓います。
        <br>
        <br>5. Indemnification.
        <br>Assignor agrees to indemnify and hold harmless Assignee for any claims, suits, damages, actions, or other costs arising out any breach of Assignor’s warranties set forth in Section 3 above.
        <br>補償譲渡人は、上記の３項に記載されている譲渡人の保証の違反に起因する請求、訴訟、損害、訴訟、またはその他の費用について、譲受人の負担を補償し無害にすることに同意します。
        <br>
        <br>6. Governing Law.
        <br>This Agreement shall be construed in accordance with, and governed in all respects by, the laws of the State of Nevada in USA, without regard to conflicts of law principles.
        <br>準拠法
        <br>本契約は抵触法の原則に関係なく米国ネバダ州の法律に従って解釈され、あらゆる点で準拠するものとします。
        <br>
        <br>7. Severability.
        <br>In the event that any part or parts of this Agreement shall be held unenforceable for any reason, the remainder of this Agreement shall continue in full force and effect as if the unenforceable part or parts were. If any provision of this Agreement is deemed invalid or unenforceable by any court of competent jurisdiction, and if limiting such provision would make the provision valid, then such provision shall be deemed to be construed as so limited.
        <br>可分性
        <br>本契約のいずれかの部分が、何らかの理由で執行不能とされた場合、本契約の残りの部分は執行不能な部分関わらずに完全に効力を持ち続けるものとします。 本契約のいずれかの条項が管轄裁判所によって無効または執行不 能と見なされ、そのような条項を制限することでその条項が有効になる場合、そのような条項はそのように制限 されていると解釈されるものとします。
        <br>
        <br>8. Counterparts.
        <br>This Agreement may be executed in several counterparts, each of which shall constitute an original and all of which, when taken together, shall constitute one agreement.
        <br>相対物
        <br>本契約は、複数の相対物で締結される場合があり、それぞれが原本を構成し、それらすべてがまとめられた場合 1つの契約を構成するものとします。
        <br>
        <br>9. Notice.
        <br>Any notice required or otherwise given pursuant to this Agreement shall be in writing and mailed certified return receipt requested, Email, or sent letter phisically to each parties, addressed as follows:
        <br>通知
        <br>本契約に従って要求またはその他の方法で用いられる通知は、書面、または要求された証明済みの領収書を郵送するか、電子メールで送信するか、各当事者に物理的に手紙を送付するものとします。
        <br>
        <br>If to Assignor address:
        <br>______________________________________________________________
        <br>If to Assignee address:
        <br>______________________________________________________________
        <br>
        <br>
        <br>10. Headings.
        <br>The headings for section herein are for convenience only and shall not affect the meaning of the provisions of this Agreement
        <br>見出し
        <br>本書の項目の見出しは便宜上のものであり、本契約の条項の意味に影響を与えるものではありません。
        <br>
        <br>11. Entire Agreement.
        <br>This Agreement constitutes the entire agreement between Assignor and Assignee, and supersedes any prior understanding or representation of any kind preceding the date of this Agreement. There are no other promises, conditions, understandings or other agreements, whether oral or written, relating to the subject matter of this Agreement.
        <br>完全合意
        <br>本契約は、譲渡人と譲受人の間の完全な合意を構成し、本契約の日付より前のあらゆる種類の事前の理解または 表明よりも優先します。 本契約の主題に関連して、口頭または書面を問わず、その他の約束、条件、理解、また はその他の合意はありません。
        <br>
        <br>IN WITNESS WHEREOF, the parties have caused this Agreement to be executed the day and year first above written.
        <br>その証人として、両当事者は、本契約を上記の最初の年月日で締結させました。
        <br>
        <br>
        <br>
        <div style="float:left; width:45%">
            <h3>ASSIGNOR</h3>
            <br>_______________
            <br>Signature
            <br>_______________
            <br>Print Name
        </div>
        <div style="float:right; width:45%">
            <h3>ASSIGNEE</h3>
            <br>_______________
            <br>Signature
            <br>_______________
            <br>Print Name
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>APPENDIX A
        <br>
        <br>(Include here a list of any licensing agreement which will survive this assignment, and attach a copy of each as Exhibits, beginning with Exhibit A)
        <br>（この割り当てを存続させるライセンス契約のリストをここに含め、それぞれのコピーをこの別紙Aから始まる別紙として添付してください）
        <br>
        <br>Product file name list here…
        <br>
        <br>XXXXX.mov
        <br>XXXXX.mp4
    </body>
</html>